edgarpull
=========

Project requirement
-------------------

Fetch required data


How to run
----------

To run simply do::

	$ python angelhack.py

*If you are using the daemon follow below*

To start the daemon ::

    $ python edgarpull.py start

this will start inserting the articles to the database(Assuming the mongodb database is running).
It will log the data inserted to the database in a file named edgarpull.log.

To stop the daemon ::

    $ python edgarpull.py stop


Press [ctrl]+c to stop the process.


Example run
-----------

    $ python angelhack.py

*For daemonised code*

    $ python edgarpull.py start
    PID: 3418 Daemon started successfully

    $ python edgarpull.py stop
    Daemon killed succesfully


